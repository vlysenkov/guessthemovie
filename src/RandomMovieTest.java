import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class RandomMovieTest {
    private ArrayList<String> movieList = new ArrayList<>();

    @BeforeEach
    public void getMovieList() {
        movieList.add("the shawshank redemption");
        movieList.add("the godfather");
        movieList.add("the dark knight");
        movieList.add("schindler's list");
        movieList.add("pulp fiction");
        movieList.add("the lord of the rings");
        movieList.add("the good the bad and the ugly");
        movieList.add("fight club");
        movieList.add("the lord of the rings");
        movieList.add("forrest gump");
        movieList.add("star wars");
        movieList.add("inception");
        movieList.add("the lord of the rings");
        movieList.add("the matrix");
        movieList.add("samurai");
        movieList.add("star wars");
        movieList.add("city of god");
        movieList.add("the silence of the lambs");
        movieList.add("batman begins");
        movieList.add("die hard");
        movieList.add("chinatown");
        movieList.add("room");
        movieList.add("dunkirk");
        movieList.add("fargo");
        movieList.add("no country for old men");
    }

    RandomMovie randomMovie = new RandomMovie();
    int triesAmount = 100;

    @Test
    void shouldMatchFirstMovie() {
        String expected = movieList.get(0);
        int amountOfTries = 1;
        for (int i = 1; i <= triesAmount; i++) {
            String actual = randomMovie.chooseRandomMovie(movieList);
            if (actual.equals(expected)) {
                break;
            }
            amountOfTries++;
            assertNotEquals(i, triesAmount);
        }
        System.out.println("Amount of tries: " + amountOfTries);
    }

    @Test
    void shouldMatchLastMovie() {
        String expected = movieList.get(movieList.size() - 1);
        int amountOfTries = 1;
        for (int i = 1; i <= triesAmount; i++) {
            String actual = randomMovie.chooseRandomMovie(movieList);
            if (actual.equals(expected)) {
                break;
            }
            amountOfTries++;
            assertNotEquals(i, triesAmount);
        }
        System.out.println("Amount of tries: " + amountOfTries);
    }
}