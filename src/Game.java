import java.util.Scanner;

public class Game {
    boolean playGame(String guessMovie) {
        boolean hasWon = false;
        String convertedGuessMovie = guessMovie.replaceAll("[a-zA-Z]", "_");
        System.out.println(convertedGuessMovie);
        Scanner scanner = new Scanner(System.in);
        for (int i = 10; i > 0; ) {
            System.out.println("You have " + i + " guess(es) left.");
            String guess = scanner.nextLine();
            if (convertedGuessMovie.contains(guess)) {
                System.out.println("You have already guessed this letter. Try another one :)");
                continue;
            }
            int indexOfLetter = guessMovie.indexOf(guess);
            if (indexOfLetter == -1) {
                System.out.println("There is no chosen letter :(");
            }
            else {
                do {
                    StringBuilder convertedGuessMovieSB = new StringBuilder(convertedGuessMovie);
                    char guessedLetter = guess.charAt(0);
                    convertedGuessMovieSB.setCharAt(indexOfLetter, guessedLetter);
                    convertedGuessMovie = convertedGuessMovieSB.toString();
                    indexOfLetter = guessMovie.indexOf(guess, indexOfLetter + 1);
                } while (indexOfLetter >= 0);
            }
            System.out.println(convertedGuessMovie);
            if (!convertedGuessMovie.contains("_")) {
                hasWon = true;
                break;
            }
            i--;
        }
        scanner.close();
        return hasWon;
    }
}
