import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class GuessTheMovie {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("movies.txt");
        Scanner sc = new Scanner(file);
        ArrayList<String> movieList = new ArrayList<>();
        while (sc.hasNextLine()) {
            String movie = sc.nextLine();
            movieList.add(movie);
        }
        sc.close();

        RandomMovie randomMovie = new RandomMovie();
        String guessMovie = randomMovie.chooseRandomMovie(movieList);

        System.out.println("I have randomly chosen a movie name.");
        System.out.println("Try to guess it.");
        Game game = new Game();
        if (game.playGame(guessMovie)) {
            System.out.println("CORRECT...YOU WIN!!!");
        } else {
            System.out.println("GAME OVER :( YOU LOSE!!!");
            System.out.println("The movie was " + guessMovie);
        }
    }
}



